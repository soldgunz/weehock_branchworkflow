﻿using System;

namespace Branch_workflow
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int num;
            int sum = 0;

            Console.WriteLine("Enter a number: ");
            num = Convert.ToInt32(Console.ReadLine());

            for (int i =1; i<= num; i++)
            {
                Console.WriteLine(i);
                sum = sum + i;
            }

            Console.WriteLine("The sum is: " + sum);
            Console.WriteLine("This is the last code");
        }
    }
}
